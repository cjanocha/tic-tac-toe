import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class TicTacToeMain extends Application{
	private static TicTacToeButton [] buttons = new TicTacToeButton[9];
	private static int buttonsLeft=9;
	private static boolean playerFirst=true;
	public static void main(String[] args){
		launch(args);
	}
	
	@Override
	public void start(Stage window) throws Exception {
		Alert alert=new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Tic Tac Toe");
		alert.setHeaderText("JFX Tic Tac Toe");
		alert.setContentText("Would you like to go first or second?");
		ButtonType buttonTypeOne = new ButtonType("One");
		ButtonType buttonTypeTwo = new ButtonType("Two");
		ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get()==buttonTypeOne){
			
		}else if (result.get()==buttonTypeTwo){
			playerFirst=false;
		}else{
			System.exit(1);
		}
			
		GridPane grid = new GridPane();
		
		
		List<TicTacToeButton> buttonsLeft = new ArrayList<TicTacToeButton>();
		
		for(int i = 0; i<buttons.length; ++i){
			buttons[i] = new TicTacToeButton("-", 0);
			buttons[i].setStyle("-fx-font-size: 25;");
		}
		
		for (int g=0; g<buttons.length; ++g){
			buttonsLeft.add(buttons[g]);
		}
		
		grid.setHgap(10);
		grid.setVgap(10);
		
		grid.setPadding(new Insets(10, 10, 10, 10));
		
		//setting button sizes
		for (int j=0; j<buttons.length; j++)
		buttons[j].setPrefSize(10000000,10000000);
		
		//adding all buttons to grid
		for(int a=0, col=0, row=0; a<buttons.length; a++, col++){
			if (col>2){
				col=0;
				row++;
			}
			grid.add(buttons[a], col, row);
		}
		
		//what happens when a button is clicked
		for (int z=0; z<buttons.length; z++){
		    final int temp=z;
		    buttons[temp].isClicked();
		}
		
        grid.setAlignment(Pos.CENTER);
		Scene scene = new Scene(grid, 300, 300);
		
		window.setScene(scene);
		if (!playerFirst){computerTurn();}
		window.show();
		
	}
	    public static void buttonsUnavailible(){ 
	    	buttonsLeft--;
	    }
	    
        public static void computerTurn(){
        	int temp=(int) (Math.random()*9);
        	if(buttonsLeft==0){
        		
        	}else if (buttons[temp].getText().equals("-")){
        		buttons[temp].setText("O");
        		buttons[temp].setDisable(true);
     		    buttons[temp].setStyle(buttons[temp].getStyle() + "-fx-opacity: 1.0; -fx-base: #FFFFFF;");
        		buttons[temp].setValue(-1);
        		buttonsUnavailible();
        		CalculateWinner();
        	}else{
        		computerTurn();
        	}
        	
        }
        public static void CalculateWinner(){
        	int hor1=buttons[0].getValue()+buttons[1].getValue()+buttons[2].getValue();
        	int hor2=buttons[3].getValue()+buttons[4].getValue()+buttons[5].getValue();
        	int hor3=buttons[6].getValue()+buttons[7].getValue()+buttons[8].getValue();
        	int vert1=buttons[0].getValue()+buttons[3].getValue()+buttons[6].getValue();
        	int vert2=buttons[1].getValue()+buttons[4].getValue()+buttons[7].getValue();
        	int vert3=buttons[2].getValue()+buttons[5].getValue()+buttons[8].getValue();
        	int diag1=buttons[0].getValue()+buttons[4].getValue()+buttons[8].getValue();
        	int diag2=buttons[2].getValue()+buttons[4].getValue()+buttons[6].getValue();
        	if(hor1==3||hor2==3||hor3==3||vert1==3||vert2==3||vert3==3||diag1==3||diag2==3){
        		Alert alert = new Alert(AlertType.CONFIRMATION);
        		alert.setTitle("!");
        		alert.setHeaderText("YOU'RE WINNER!");
        		alert.showAndWait();
        		System.exit(1);
        	}else if(hor1==-3||hor2==-3||hor3==-3||vert1==-3||vert2==-3||vert3==-3||diag1==-3||diag2==-3){
        		Alert alert = new Alert(AlertType.CONFIRMATION);
        		alert.setTitle("!");
        		alert.setHeaderText("YOU'RE LOSER!");
        		alert.showAndWait();
        		System.exit(1);
        	}else if (buttonsLeft==0){
        		Alert alert = new Alert(AlertType.CONFIRMATION);
        		alert.setTitle("!");
        		alert.setHeaderText("NO ONE WINS!");
        		alert.showAndWait();
        		System.exit(1);
        		
        	}
        }
}