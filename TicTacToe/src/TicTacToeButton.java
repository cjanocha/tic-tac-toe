import javafx.scene.control.Button;

public class TicTacToeButton extends Button{
	String name;
	int value;
	
	public TicTacToeButton (String name, int value){
		this.name=name;
		this.setText(name);
		this.value=value;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public void setValue(int newVal){
		this.value=newVal;
	}
	
	public void isClicked(){
		this.setOnAction(e ->{
			this.setText("X");
		    this.setDisable(true);
		    this.setStyle(this.getStyle() + "-fx-opacity: 1.0; -fx-base: #FFFFFF;");
		    this.setValue(1);
		    TicTacToeMain.buttonsUnavailible();
		    TicTacToeMain.CalculateWinner();
		    TicTacToeMain.computerTurn();
		    
		});
	}

	

}
